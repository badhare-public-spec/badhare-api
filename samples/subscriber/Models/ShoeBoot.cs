﻿namespace BadHare.SubscriberExample.Models;

public record ShoeBoot(string TableName, DateTime DateTimeUtc)
    : BaseRecord(TableName, DateTimeUtc)
{
    public const string MessageType = "CasinoBlackjackBootupItem";
}