﻿namespace BadHare.SubscriberExample.Models;

public abstract record BaseRecord(string TableName, DateTime DateTimeUtc);