﻿namespace BadHare.SubscriberExample.Models;

public record ShoeInfo(string TableName, DateTime DateTimeUtc, TimeSpan ShoeUpTime, TimeSpan CurrentShoeTime,
        int CardsDrawn, int CardsRemaining, int RoundCount)
    : BaseRecord(TableName, DateTimeUtc)
{
    public const string MessageType = "CasinoBlackjackShoeInfoItem";
}