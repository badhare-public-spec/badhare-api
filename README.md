# Bad Hare SDK/API

Welcome to the Bad Hare API/SDK page. Please see the below links:

[Message Specification](BusMessages.md).

## Examples

<em>Note: The dotnet6 examples makes use of the new `record` type. This is very similar to a class, just with less lines of code. The Constructor names are the same as standard properties, so if you don't have access to create records, you can create the same thing in a standard class.</em>

![C# 6 Console App Example](/images/SampleProgram.png)

[C# Dotnet6 Example Source](https://gitlab.com/badhare-public-spec/badhare-api/-/blob/main/samples/subscriber/BusService.cs)

