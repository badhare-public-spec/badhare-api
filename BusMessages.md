# BadHare-Api

API Specification Docs

## Message Transport
Bad Hare messages are passed via message bus, specificity amqp(s) connections. Bad Hare uses RabbmitMq as a bus, but any amqps connection should work just fine.  Data can be transported to any number of apps real time via this open data design.

![Transport Example](/images/transport0.png)

Example sending to both a private casino store or application as well as one of the Bad Hare partners eConnect

![Transport Example](/images/transport1.png)

## Message Encoding
All messages that are published to the message bus use MessagePack Encoding. 

## Message Routing
All messages are published to a message bus topic exchange.  The routing keys are as followed:

`casino.<gametype>.<tablename>.<event>`

## Messages
All messages will include the following values:
- TableName: `string` - The configured name on the device that is doing the publishing
- DateTimeUtc: `datetime / ISO8601` - The date/time of the publish from the device

## Boot-Up
When a device has finished booting, it will indicate to the bus that it has started. 

Routing key:
`casino.blackjack.<tablename>.bootup`

Message Type: `CasinoBlackjackBootupItem`

Example Payload:

```
{
   "TableName":"BH-06",
   "DateTimeUtc":"2021-06-25T01:09:53.287Z"
}
```

## Liveness
Every shoe that is online will publish a message to bus advertising that it is online.

Routing key:
`casino.blackjack.<tablename>.liveness`

Message Type: `CasinoBlackjackShoeInfoItem`

Example Payload:
```
{
   "TableName":"BH-06",
   "DateTimeUtc":"2021-06-25T00:50:08.310Z",
   "ShoeUpTime":31463381731350,
   "CurrentShoeTime":253969980176,
   "CardsDrawn":22,
   "CardsRemaining":82,
   "RoundCount":2
}
```
### Fields
- ShoeUpTime: `number` - The number of milliseconds uptime since the shoe was last restarted
- CurrentShoeTime: `number` - The number of milliseconds the current has been running shoe since it was reset (shuffle - button press)
- CardsDrawn: `number` - Number of cards drawn since shuffle
- CardsRemaining: `number` - number of cards remaining in the shoe
- RoundCount: `number` - Estimated number of rounds detected, based on the delay algorithm programmed in the device (idle time between draws)

## Start Of Shoe
Every time the shoe is reset / shuffled, an event message will be passed. This happened when the user presses the button on the shoe to indicate a shuffle.

Routing key:
`casino.blackjack.<tablename>.startofshoe`

Message Type: `CasinoBlackjackStartOfShoeItem`

Example Payload:

```
{
   "TableName":"BH-06",
   "DateTimeUtc":"2021-06-25T01:03:07.976Z",
   "DeckCount":2,
   "ShoeDeckId":637601797879752600
}
```

### Fields
- DeckCount: `number` - Number of decks programmed for this shoe
- ShoeDeckId: `number` - Unique number when the shoe was reset, that can be used to track the cards associated to the decks in the shoe

## New Round
Each time a round ends (based on timing of the last card drawn), a message will indicate a new round is ready.

Routing key:
`casino.blackjack.<tablename>.newround`

Message Type: `CasinoBlackjackNewRoundItem`

Example Payload:

```
{
   "TableName":"BH-06",
   "DateTimeUtc":"2021-06-25T01:03:25.811Z",
   "ShoeDeckId":637601797879752600
}
```
### Fields
- ShoeDeckId: `number` - Unique number when the shoe was reset, that can be used to track the cards associated to the decks in the shoe

## Card Drawn
Every card that is drawn, will result in a prediction and published as soon as its known.

Routing key:
`casino.blackjack.<tablename>.carddraw`

Message Type: `CasinoBlackjackDrawItem`

Example Payload:

```
{
   "TableName":"BH-06",
   "DateTimeUtc":"2021-06-25T01:03:11.629Z",
   "BurnCard":false,
   "CardRank":107,
   "CardSuit":104,
   "CardCount":4,
   "ShoeDeckId":637601797879752600,
   "CardDrawSpeedMs":99
}
```
### Fields
- BurnCard: `bool` - If the card is a burn card, return true (currently not implemented)
- CardRank: `byte` - Ascii char code for 
  - k (`107`)
  - h (`104`)
  - s (`115`)
  - c (`99`)
- CardSuit: `byte` - Ascii char code for card suit
  - 2 (`50`)
  - 3 (`51`)
  - 4 (`52`)
  - 5 (`53`)
  - 6 (`54`)
  - 7 (`55`)
  - 8 (`56`)
  - 9 (`57`)
  - T (`84`)
  - j (`106`)
  - q (`113`)
  - k (`107`)
  - a (`97`)
- ShoeDeckId: `number` - Unique number when the shoe was reset, that can be used to track the cards associated to the decks in the shoe

## Low Level Example

Example of a low level payload with the headers, content, and properties

```
{
   "fields":{
      "consumerTag":"amq.ctag-hfrfcD4Eii14qEbBxU8Jyg",
      "deliveryTag":98,
      "redelivered":false,
      "exchange":"datacollection.tx",
      "routingKey":"casino.blackjack.bh-06.liveness"
   },
   "properties":{
      "contentType":"application/msgpack",
      "headers":{
         "CC":[
            
         ],
         "timestamp_in_ms":1624583479626
      },
      "deliveryMode":1,
      "timestamp":1624583479,
      "type":"CasinoBlackjackShoeInfoItem"
   },
   "content":{
      "TableName":"BH-06",
      "DateTimeUtc":"2021-06-25T01:11:19.614Z",
      "ShoeUpTime":1189957251,
      "CurrentShoeTime":888903408,
      "CardsDrawn":0,
      "CardsRemaining":104,
      "RoundCount":0
   },
   "_msgid":"c0fe1712.df17b8"
}
```

Every capture of information comes with additional metadata.  This metadata can also be quite helpful.

In the properties of the metadata, you can find several helpful types:
 - `contentType`: - The encoded message type
 - `headers`: - Future versions may have a modified routing key, so the message could be copied to other routing keys
 - `timestamp` - The epoch time the message was received. Could be handy if the device itself 
 - `type` - When the message is published, we publish with additional data on the type of message. This is helpful if you were to capture all messages and instead switch on the message type in your own code. 

