﻿namespace BadHare.SubscriberExample.Models;

public record ShoeError(string TableName, DateTime DateTimeUtc, int ErrorCode, string ErrorMessage)
    : BaseRecord(TableName, DateTimeUtc)
{
    public const string MessageType = "CasinoBlackjackErrorItem";
}