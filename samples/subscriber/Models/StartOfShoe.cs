﻿namespace BadHare.SubscriberExample.Models;

public record StartOfShoe(string TableName, DateTime DateTimeUtc, int DeckCount, long ShoeDeckId) : BaseRecord(
    TableName, DateTimeUtc)
{
    public const string MessageType = "CasinoBlackjackStartOfShoeItem";
}