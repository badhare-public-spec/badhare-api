﻿namespace BadHare.SubscriberExample.Models;

public record NewRound(string TableName, DateTime DateTimeUtc, long ShoeDeckId) : BaseRecord(TableName, DateTimeUtc)
{
    public const string MessageType = "CasinoBlackjackNewRoundItem";
}