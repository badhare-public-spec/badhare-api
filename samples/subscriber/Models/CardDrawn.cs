﻿namespace BadHare.SubscriberExample.Models;

public record CardDrawn(string TableName, DateTime DateTimeUtc, bool BurnCard, char CardRank, char CardSuit, int CardCount, long ShoeDeckId, long CardDrawnSpeedMs) 
    : BaseRecord(TableName, DateTimeUtc)
{
    public const string MessageType = "CasinoBlackjackDrawItem";
}