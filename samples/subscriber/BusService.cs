﻿using BadHare.SubscriberExample.Models;
using MessagePack;
using MessagePack.Resolvers;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BadHare.SubscriberExample;

internal class BusService : IHostedService
{
    /// <summary>
    /// Create a queue that you would like to persist data with.  If you plan to have a central service capture all data, consider a queue name unique the service
    /// consuming it such as CasinoBlackJack_Service1 - whatever fits your naming convention
    /// If you are disconnected, you can have this queue collect data until you reconnect again. This is setup later in the example.
    /// </summary>
    private const string queueName = "DEMOSDK_Data";

    /// <summary>
    /// The shoe is configurable with whatever topic exchange name is desired for the property. The default value of datacollection.tx is configured by default
    /// </summary>
    private const string exchangeName = "datacollection.tx";

    /// <summary>
    ///     Routing Key format for each message follows:
    ///     casino.blackjack.[tablename].[event]
    ///     The example here, will receive all messages under this casino.blackjack route
    ///     You can request a single table by like casino.blackjack.MyTable 123.#
    ///     You can even use a wildcard in place of a value such as casino.blackjack.*.carddrawn  (this will give you only card drawns but for every table)
    /// </summary>
    private const string routingKey = "casino.blackjack.#";

    private readonly ILogger<BusService> _logger;
    private IModel? _channel;

    private IConnection? _connection;

    public BusService(ILogger<BusService> logger)
    {
        _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        // Setup a connection factory. The URL is unique to each property with a different password, address and vhost. This example
        // uses a cloud based one for development purposes.
        var connectionFactory = new ConnectionFactory
        {
            Uri = new Uri("amqps://badhare:718d51a981b90e6d5a867b18f640784c0630@badhare.econnectcloud.com:5672/badhare")
        };

        _logger.LogInformation("Connecting...");
        _connection = connectionFactory.CreateConnection();

        _logger.LogInformation("Creating Channel...");
        _channel = _connection.CreateModel();

        // This is where you will declare the queue properties.  You can even make it create a temporary queue while you are connected, and when you disconnect 
        // it removes the queue automaticly.  In this case, we are using a persistent queue which will survive re-connections
        _logger.LogInformation($"Declaring Queue '{queueName}'...");
        var queueOk = _channel.QueueDeclare(queueName, true, false, false, null);

        // Bind will connect the data exchange with the queue that was created, using the routing key pattern that you have defined.
        _logger.LogInformation(
            $"Binding to Queue '{queueName}' for exchange '{exchangeName}' using routing key '{routingKey}'.");
        _channel.QueueBind(queueName, exchangeName, routingKey);

        // Create the consumer. 
        var consumer = new EventingBasicConsumer(_channel);

        // Create a callback for when messages are received
        consumer.Received += Consumer_Received;

        // Start the consumption of messages from the bus
        _channel.BasicConsume(queueName, true, consumer);

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        // When disconnecting, make sure to dispose your resources
        _channel?.Dispose();
        _connection?.Dispose();

        return Task.CompletedTask;
    }

    private void Consumer_Received(object? sender, BasicDeliverEventArgs e)
    {
        _logger.LogInformation($"Message Received: {e.RoutingKey}");

        // BadHare uses a Contractless MessagePack publish. This is important to resolve the object.
        var options = ContractlessStandardResolver.Options;

        switch (e.BasicProperties.Type)
        {
            case CardDrawn.MessageType:
                var cardDrawn = MessagePackSerializer.Deserialize<CardDrawn>(e.Body, options);
                _logger.LogInformation(
                    $"[{cardDrawn.TableName}:{cardDrawn.DateTimeUtc}] Draw: {cardDrawn.CardRank}:{cardDrawn.CardSuit}; Burn: {cardDrawn.BurnCard}; Card drawn time: {cardDrawn.CardDrawnSpeedMs}; Deck Id: {cardDrawn.ShoeDeckId}");
                break;
            case NewRound.MessageType:
                var newRound = MessagePackSerializer.Deserialize<NewRound>(e.Body, options);
                _logger.LogInformation(
                    $"[{newRound.TableName}:{newRound.DateTimeUtc}] Round: Deck Id: {newRound.ShoeDeckId}");
                break;
            case ShoeBoot.MessageType:
                var shoeBoot = MessagePackSerializer.Deserialize<ShoeBoot>(e.Body, options);
                _logger.LogInformation($"[{shoeBoot.TableName}:{shoeBoot.DateTimeUtc}] Booted");
                break;
            case ShoeError.MessageType:
                var shoeError = MessagePackSerializer.Deserialize<ShoeError>(e.Body, options);
                _logger.LogInformation(
                    $"[{shoeError.TableName}:{shoeError.DateTimeUtc}] Error Code: {shoeError.ErrorCode}; Error Message: {shoeError.ErrorMessage}");
                break;
            case ShoeInfo.MessageType:
                var shoeInfo = MessagePackSerializer.Deserialize<ShoeInfo>(e.Body, options);
                _logger.LogInformation(
                    $"[{shoeInfo.TableName}:{shoeInfo.DateTimeUtc}] Info: Cards Drawn: {shoeInfo.CardsDrawn}; Cards Remaining: {shoeInfo.CardsRemaining}; Current Shoe Time: {shoeInfo.CurrentShoeTime}; Round Count: {shoeInfo.RoundCount}; Shoe Up time: {shoeInfo.ShoeUpTime}");
                break;
            case StartOfShoe.MessageType:
                var startOfShoe = MessagePackSerializer.Deserialize<StartOfShoe>(e.Body, options);
                _logger.LogInformation(
                    $"[{startOfShoe.TableName}:{startOfShoe.DateTimeUtc}] Start of Shoe: Deck Count: {startOfShoe.DeckCount}; Deck Id: {startOfShoe.ShoeDeckId}");
                break;
            default:
                _logger.LogInformation($"MessageType: '{e.BasicProperties.Type}' not supported");
                break;
        }
    }
}